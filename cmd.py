from epson import EpsonProjCmd
import time
import sys
import configparser

cmd = ""
if len(sys.argv) >= 2:
    cmd = sys.argv[1]
print("========================")
print("example: python cmd.py [cmd]")
print("cmd: p_on, p_off, s_change")
print("-------------------")
print("cmd:" + cmd)

projectors = []

#--------------------- config
config = configparser.ConfigParser()
config.read("p.ini")

if config["proj1"]["ip"] != "":
    projectors.append(EpsonProjCmd(config["proj1"]["ip"]))
    print("append projector:" + config["proj1"]["ip"])
if config["proj2"]["ip"] != "none":
    projectors.append(EpsonProjCmd(config["proj2"]["ip"]))
    print("append projector:" + config["proj2"]["ip"])
print("number of projectors: " + str(len(projectors)))

#--------------------- command
for p in projectors:
    res = p.handShake()
    print(res)
time.sleep(2.0)

if cmd == "p_off":
    for p in projectors:
        res = p.powerOff()
        print(res)
elif cmd == "p_on":
    for p in projectors:
        res = p.powerOn()
        print(res)
elif cmd == "s_change":
    for p in projectors:
        res = p.sourceChange()
        print(res)
time.sleep(2.0)
for p in projectors:
    res = p.close()
print("Done")
print("========================")