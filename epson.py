import socket
import time 
import threading

"""
エプソンプロジェクターをコマンドで操作する。

内部手順：
・tcpによる接続
・handshakeコマンド送信
・各種コマンド送信
・close処理
リアルタイムに行う処理でもないので、処理の間にはインターバルを入れて確実性を増しつつ、都度接続する。

コマンドリスト：
https://files.support.epson.com/pdf/pltw1_/pltw1_cm.pdf
https://www.epson.jp/products/download/elp/escvp21_kyodaku.htm （ダウンロードページ）
通信手順：
https://www.epson.eu/viewcon/corporatesite/kb/index/1912
hex-ascii変換参考：
http://web-apps.nbookmark.com/ascii-converter/

"""
class EpsonProjCmd(object):
    def __init__(self, ip, port=3629):
        self.handShakeCmd = b'\x45\x53\x43\x2F\x56\x50\x2E\x6E\x65\x74\x10\x03\x00\x00\x00\x00' #ESC/VP.net
        self.powerOnCmd = b'\x50\x57\x52\x20\x4f\x4e\x0d'   #PWR ON?
        self.powerOffCmd = b'\x50\x57\x52\x20\x4f\x46\x46\x0d'    # PWR OFF?
        self.sorceChangeCmd = b'\x53\x4f\x55\x52\x43\x45\x20\x31\x30\x0d' # SOURCE 10? コンピュータには切り替わった
        self.projIp = "192.168.0.15"
        self.projPort = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.projIp, self.projPort))
        return
    def handShake(self):
        self.socket.sendall(self.handShakeCmd)
        res = self.socket.recv(1024)
        return res
    def powerOn(self):
        self.socket.sendall(self.powerOnCmd)
        res = self.socket.recv(1024)
        return res
    def powerOff(self):
        self.socket.sendall(self.powerOffCmd)
        res = self.socket.recv(1024)
        return res
    def sourceChange(self):
        self.socket.sendall(self.sorceChangeCmd)
        res = self.socket.recv(1024)
        return res
    def close(self):
        res = self.socket.close()
        return res