Epson Projector Control app
==============

## Overview
エプソンのプロジェクターを制御するためのミニアプリです。

## Description
現在は電源のon/off, 入力先の切り替え（ソース1のみ）に対応しています。

プロジェクター情報をp.iniファイルに記載してください。
<pre>
[proj1]
ip="192.168.0.15"
[proj2]
ip=none
</pre>
接続先プロジェクターのIPアドレスを指定します。
現在は2台接続する想定です。
必要がない場合はnoneと記述してください。

## Usage

```
python cmd.py [コマンド]
```
[コマンド]リスト
* p_on: 電源on
* p_off: 電源off
* s_change: 入力切替(ソース1)

## Licence

[MIT](https://github.com/tcnksm/tool/blob/master/LICENCE)

## Author

[Mitsuru Takeuchi](https://hi-farm.net)